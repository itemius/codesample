import UIKit
import GoogleMaps
import GooglePlaces
import os.log
import InteractiveSideMenu
import FSCalendar
import EventKit
import UserNotifications
import Flurry_iOS_SDK

class CalendarViewController: UIViewController, GMSMapViewDelegate, SideMenuItemContent, FSCalendarDataSource, FSCalendarDelegate  {
    
    var calendar: Calendar?
    var today: Date?
    var selected: Date?
    var buttons: [DateButton] = []
    var events: [MAEvent] = []
    
    var eventLastPlace: GMSPlace?
    
    var eventsListTableViewController: EventsListTableViewController?
    var statusOverlay = ResourceStatusOverlay()
    
    var isNotUpdated = true
    
    var routeInteractor = RouteInteractor()
    
    let eventInteractor = EventInteractor()
    
    @IBOutlet weak var currentMonthLabel: UILabel!    
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var whiteLine: UIView!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    
    // MARK: - Private methods -
    
    private func loadEvents(date: Date) {
        
        eventInteractor.loadEvents(date: date) { events in
            if events != nil {
                self.events = events!
                
                self.eventsListTableViewController?.tableView.reloadData()
            } else {
                self.showAlert(withTitle: "", message: NSLocalizedString("Error loading events", comment: "Error loading events"))
            }
        }
    }
    
    private func setCurrentMonthLabel() {
        
        let components = calendar?.dateComponents(in: .current, from: today!)
        let dateFormatter = DateFormatter()
        
        let month = dateFormatter.monthSymbols[(components?.month)! - 1]
        let year = String((components?.year)!)
        
        currentMonthLabel.text = month.capitalizingFirstLetter() + " " + year
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        calendar = Calendar.current
        today = Date()        
        calendarView.scope = .week
                                
        calendarView.select(today!)
        selected = today!
        containerView.superview?.bringSubview(toFront: containerView)
        whiteLine.superview?.bringSubview(toFront: whiteLine)
        
        checkCalendarPermission()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.becomeForeground),
            name: NSNotification.Name.UIApplicationWillEnterForeground,
            object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        Flurry.logEvent("Agenda_opened", withParameters: nil, timed: true);
        
        MAMapsPlacesService.attachMap(view, container: self)
        statusOverlay.embed(in: self)
        
        checkCalendarPermission()
        
        setCurrentMonthLabel()
    }

    //check permissions
    func checkCalendarPermission() {
        let eventStore = EKEventStore()
        eventStore.requestAccess(to: EKEntityType.event, completion: {
            (accessGranted: Bool, error: Error?) in
            if accessGranted {
                self.settingsView.isHidden = true
            } else {
                self.settingsView.isHidden = false
            }
            self.loadEvents(date: self.selected!)
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Flurry.endTimedEvent("Agenda_opened", withParameters: nil);
    }
    
    override func viewDidLayoutSubviews() {
        
        instantiateControls()
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func becomeForeground() {
        loadEvents(date: selected!)
        setCurrentMonthLabel()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        
        let components = self.calendar?.dateComponents(in: .current, from: date)
        let dateFormatter = DateFormatter()
        
        let month = dateFormatter.monthSymbols[(components?.month)! - 1]
        let year = String((components?.year)!)
        
        currentMonthLabel.text = month.capitalizingFirstLetter() + " " + year
        selected = date
        print(date)
        loadEvents(date: date)

    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        
        print("swipe month: first day of week - " + calendar.currentPage.stringRepresentation())
        
        let components = self.calendar?.dateComponents(in: .current, from: calendar.currentPage)
        let dateFormatter = DateFormatter()
        
        let month = dateFormatter.monthSymbols[(components?.month)! - 1]
        let year = String((components?.year)!)
        
        currentMonthLabel.text = month.capitalizingFirstLetter() + " " + year
        
        calendar.select(calendar.currentPage)
        selected = calendar.currentPage
        loadEvents(date: calendar.currentPage)
    }

    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return Colors.redPink
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        return Colors.redPink
    }
    
    
    // MARK: - Navigation -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        if let viewController = segue.destination as? EventsListTableViewController, segue.identifier == "embedEventsListTableViewController" {
            
            eventsListTableViewController = viewController
        }
        
        switch(segue.identifier ?? "") {
            
        case "embedEventsListTableViewController":
            os_log("Table embedded.", log: OSLog.default, type: .debug)
            
        case "addNewEvent":
            os_log("Adding a new event.", log: OSLog.default, type: .debug)
            
            guard let eventDetailViewController = segue.destination as? EventDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            eventDetailViewController.previousPlace = eventLastPlace
            eventDetailViewController.calendarSelectedDate = selected
            eventLastPlace = nil

        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }

    @IBAction func unwindToCalendarPage(sender: UIStoryboardSegue) {

        if let eventDetailViewController = sender.source as? EventDetailViewController {
            print("returned from eventdetail")
        }
        
    }
    
    func instantiateControls() {
        
        if buttons.count == 0 {
        
            var week: [Date] = []
            let startOfWeek = today?.startOfWeek
            
            for idx in 0...6 {
                let date = calendar?.date(byAdding: .day, value: idx, to: startOfWeek!)
                week.append(date!)
            }
        
        }
        
    }
    
    func createDayButton(_ date: Date, isActive: Bool, index: Int) -> DateButton {
        
        let button = DateButton(type: UIButtonType.custom)
        button.frame = CGRect(x: 8, y: 0, width: 31, height: 31)
        
        let components = calendar!.dateComponents(in: .current, from: date)
        
        button.setTitle("\(components.day!)", for: .normal)
        button.setTitleColor(isActive ? Colors.redPink : Colors.darkGrey, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: isActive ? UIFontWeightBold : UIFontWeightRegular)
        button.setBackgroundImage(isActive ? ElementsBackgrounds.activeDayBtnBg : nil, for: .normal)
        
        button.date = date
        button.addTarget(self, action: #selector(self.dayBtnTouched(_:)), for: .touchUpInside)
        
        return button
        
    }
    
    @IBAction func menuBtnTouch(_ sender: RoundShadowButton) {
        
        showSideMenu()
        
    }
    
    @IBAction func addEventBtnTouch(_ sender: UIButton) {
        routeInteractor.getRouteDepartureArrivalAddress(eventIndex: events.count, events: events) {
            result -> Void in
            self.eventLastPlace = result?.routeAddress
            self.performSegue(withIdentifier: "addNewEvent", sender: self)
        }
    }
    
    func dayBtnTouched(_ sender: DateButton!) {
        
        today = Date()
        
        buttons.forEach { button in
            
            let isActive = (calendar?.compare(button.date, to: today!, toGranularity: .day) == .orderedSame)
            button.setTitleColor(isActive ? Colors.redPink : Colors.darkGrey, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: isActive ? UIFontWeightBold : UIFontWeightRegular)
            button.setBackgroundImage(isActive ? ElementsBackgrounds.activeDayBtnBg : nil, for: .normal)
            
        }
        
        loadEvents(date: today!)
        
    }
    
    @IBAction func settingsBtnTouch(_ sender: UIButton) {
        
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)")
            })
        }
        
    }
    
    func openSettingsAlert() {
        let alertController = UIAlertController (title: "", message: "Calendar access is turned off in your device settings, so it will not be synchronized with My Anatol", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
