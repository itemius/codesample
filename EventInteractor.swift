import Foundation
import EventKit

class EventInteractor {
    
    let eventCalendar = EventCalendar()
    let eventStorageManager = EventStorageManager()
    
    //Returns conclusive event list.
    //Composed from myanatol server events and apple calendar events.
    func loadEvents(date: Date, completion: @escaping ([MAEvent]?) -> Void) {
        eventCalendar.loadEvents(date: date, dateOnly: true) { events in
            completion(events)
        }
    }
    
    //Returns conclusive event list. Selects events from datetime to datetime + 24 h
    //Composed from myanatol server events and apple calendar events.
    func loadEvents(datetime: Date, completion: @escaping ([MAEvent]?) -> Void) {
        eventCalendar.loadEvents(date: datetime, dateOnly: false) { events in
            completion(events)
        }
    }
    
    //Delete event from server.
    func deleteMyAnatolCalendarEvent(event: MAEvent?, completion: @escaping () -> Void) {
        eventStorageManager.deleteMyAnatolCalendarEvent(event: event) {
            completion()
        }
    }
    
    //Store event at server.
    //Creates or updates existed event.
    func storeMyAnatolCalendarEvent(event: MAEvent, completion: @escaping () -> Void) {
        eventStorageManager.storeMyAnatolCalendarEvent(event: event) {
            completion()
        }
    }
    
    //Store event at apple calendar.
    func storeAppleCalendarEvent(title: String, startDatetime: Date?, endDatetime: Date?, place: String?, completion: @escaping (String?) -> Void) {
        eventStorageManager.storeAppleCalendarEvent(title: title, startDatetime: startDatetime, endDatetime: endDatetime, place: place) { commonId in
            completion(commonId)
        }
    }
    
    //Udate event at apple calendar.
    func updateAppleCalendarEvent(ekId: String, title: String, startDatetime: Date?, endDatetime: Date?, place: String?, completion: @escaping (String?) -> Void) {
        eventStorageManager.updateAppleCalendarEvent(ekId: ekId, title: title, startDatetime: startDatetime, endDatetime: endDatetime, place: place) { commonId in
            completion(commonId)
        }
    }
    
    //Get address from first event in array with place or route destination.
    func getFirstEventWithPlaceAddress(events: [MAEvent]) -> String {
        return eventCalendar.getFirstEventWithPlaceAddress(events: events)
    }

}
