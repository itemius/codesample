import Foundation
import UIKit
import EventKit

class EventCalendar {
    
    private func filterDuplicateEvents(events: [EKEvent]) -> [EKEvent] {
        var filteredEvents = [EKEvent]()
        
        for event in events {
            var isDuplicate = false
            for newEvent in filteredEvents {
                if event.eventIdentifier == newEvent.eventIdentifier {
                    isDuplicate = true
                }
            }
            if !isDuplicate {
                filteredEvents.append(event)
            }
        }
        
        return filteredEvents
    }
    
    private func sortEvents(events: [MAEvent]) -> [MAEvent] {
        return events.sorted(by: { ($0.startDatetime?.value)! < ($1.startDatetime?.value)! })
    }
    
    private func loadAppleEvents(date: Date, completion: @escaping ([MAEvent]?) -> Void) {
        let eventStore = EKEventStore()
        
        let appleCalendar = eventStore.calendars(for: .event)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        // Create start and end date NSDate instances to build a predicate for which events to select
        var startDate = date
        let startString = dateFormatter.string(from: startDate)
        startDate = dateFormatter.date(from: startString)!
        var endDate = date.tomorrow!
        
        let cal = Calendar(identifier: .gregorian)
        endDate = cal.startOfDay(for: endDate)
        
        print("load apple events: start - " + startDate.stringRepresentation() + "; end - " + endDate.stringRepresentation())
        // Use an event store instance to create and properly configure an NSPredicate
        let eventsPredicate = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: appleCalendar)
        
        // Use the configured NSPredicate to find and return events in the store that match
        var eventsApple = eventStore.events(matching: eventsPredicate).sorted(){
            (e1: EKEvent, e2: EKEvent) -> Bool in
            return e1.startDate.compare(e2.startDate) == ComparisonResult.orderedAscending
        }
        print("apple events")
        
        convertAppleEvents(eventsApple: eventsApple) { eventsList in
            completion(eventsList)
        }
    }
    
    private func convertAppleEvents(eventsApple: [EKEvent], completion: @escaping ([MAEvent]?) -> Void) {
        var eventsApple = eventsApple
        var eventsList = [MAEvent]()
        let group = DispatchGroup()
        
        eventsApple = filterDuplicateEvents(events: eventsApple)
        
        for event in eventsApple{
            if (event.location?.isEmpty)! {
                let maevent = MAEvent(caption: event.title, commonEventId: event.eventIdentifier, gmsPlace: nil, start: event.startDate, end: event.endDate, eventDetailRoute: nil)
                eventsList.append(maevent)
                print("title: " + event.title, "id: " + event.eventIdentifier)
            } else {
                group.enter()
                
                MAMapsPlacesService.loadPlaceFromAppleEvent(eventId: event.eventIdentifier, completion: {result in
                    if result != nil {
                        let maevent = MAEvent(caption: event.title, commonEventId: event.eventIdentifier, gmsPlace: result, start: event.startDate, end: event.endDate, eventDetailRoute: nil)
                        eventsList.append(maevent)
                        print("loaded title: " + event.title, "id: " + event.eventIdentifier)
                        group.leave()
                    } else {
                        let maevent = MAEvent(caption: event.title, commonEventId: event.eventIdentifier, gmsPlace: nil, start: event.startDate, end: event.endDate, eventDetailRoute: nil)
                        eventsList.append(maevent)
                        print("title: " + event.title, "id: " + event.eventIdentifier)
                        group.leave()
                    }
                })
            }
        }
        group.notify(queue: .main) {
            print("apple events count " + eventsList.count.description)
            completion(eventsList)
        }

    }
    
    func loadEvents(date: Date, dateOnly: Bool, completion: @escaping ([MAEvent]?) -> Void) {
        
        ApiDataService.loadEvents(date: date, dateOnly: dateOnly) { response in
            switch response.result {
                
            case .success(let events):
                
                let serverEvents = events
                
                var appleEvents = [MAEvent]()
                var allEvents = [MAEvent]()
                
                self.loadAppleEvents(date: date, completion: { result in
                    appleEvents = result!
                    
                    allEvents = self.mergeEvents(serverEvents: serverEvents, appleEvents: appleEvents)
                    
                    allEvents = self.sortEvents(events: allEvents)
                    
                    completion(allEvents)
                })
                
            case .failure(let error):
                
                print("ERROR: \(error)")
                
                completion(nil)
            }
        }
    }

    private func mergeEvents(serverEvents: [MAEvent], appleEvents: [MAEvent]) -> [MAEvent] {

        var allEvents = [MAEvent]()
        
        for serverEvent in serverEvents {
            if serverEvent.commonEventId != "" {
                var isDuplicate = false
                var event = serverEvent
                for appleEvent in appleEvents {
                    if(appleEvent.commonEventId == serverEvent.commonEventId){
                        isDuplicate = true
                        event.title = appleEvent.title
                        event.startDatetime = appleEvent.startDatetime
                        event.endDatetime = appleEvent.endDatetime
                        event.place = appleEvent.place
                    }
                }
                if isDuplicate {
                    print("duplicate " + serverEvent.title)
                    allEvents.append(event)
                    isDuplicate = false
                } else {
                    if serverEvent.commonEventId != MAEvent.notSynchronizedId {
                        MADataService.event(serverEvent._id).request(.delete)
                    } else {
                        allEvents.append(serverEvent)
                    }
                }
            } else {
                allEvents.append(serverEvent)
            }
        }
        
        for appleEvent in appleEvents {
            var isDuplicate = false
            for serverEvent in serverEvents {
                if(appleEvent.commonEventId == serverEvent.commonEventId){
                    isDuplicate = true
                }
            }
            if isDuplicate {
                isDuplicate = false
            } else {
                allEvents.append(appleEvent)
            }
        }

        return allEvents
    }
    
    func getFirstEventWithPlaceAddress(events: [MAEvent]) -> String {
        var foundPlace = false
        var firstEventWithPlace = MAEvent()
        for event in events {
            
            if event.place != nil || event.route != nil {
                foundPlace = true
                firstEventWithPlace = event
                break
            }
        }
        
        if foundPlace == true {
            var address = ""
            if firstEventWithPlace.place != nil {
                address = (firstEventWithPlace.place?.address)!
            } else {
                address = (firstEventWithPlace.route?.destination?.address)!
            }
            return address
        } else {
            return ""
        }
    }
    
}
